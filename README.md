# mintstick

A GUI to write .img or .iso files to a USB Key. It can also format them.

http://packages.linuxmint.com/pool/main/m/mintstick

**NOTE:** You need **python-pyparted** to install mintstick
<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/iso-to-usb/mintstick.git
```
